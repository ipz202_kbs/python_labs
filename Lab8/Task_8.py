import module_shop

all_store = module_shop.Shop("Meat_king", "meat")
all_store.describe_shop()


class Discount(module_shop.Shop):
    __discount_products = ['chicken', 'beef', 'pork']

    def get_discount_products(self):
        print(self.__discount_products)


"""
class Discount(Shop):
    __discount_products = ['product1', 'product2', 'product7']

    def get_discount_products(self):
        print(self.__discount_products)


store = Discount("Acent", "shoes")
store.get_discount_products()



store = Shop("Store_1", "prod_1")
store.describe_shop()
store.open_shop()


store_1 = Shop("Me_Boutique", "girls_clothes")
store_2 = Shop("you_car", "details_for_car")
store_3 = Shop("Milky_party", "milk_eat")
store_1.describe_shop()
store_2.describe_shop()
store_3.describe_shop()

store = Shop("Meat_king", "meat")
store.describe_shop()
store.open_shop()
store.set_number_of_units(200)
print(store.number_of_units)
store.increment_number_of_units(100)
print(store.number_of_units)


store = Shop("Accent", "Shoes")
store.describe_shop()
store.open_shop()
print(store.number_of_units)
store.number_of_units = 120
print(store.number_of_units)

"""
