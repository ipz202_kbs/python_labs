class Check_name_len(ValueError):
    def __init__(self, *args):
        if args:
            self.message = args[0]
            return
        self.message = None

    def __str__(self):
        if len(self.message):
            return format(self.message)


name = "Б"
if len(name) < 10:
    raise Check_name_len(name)

name = "Клімчук Богдан Станіславович"
if len(name) < 10:
    raise Check_name_len(name)

