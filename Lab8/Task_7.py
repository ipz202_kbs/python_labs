class ToRoman:
    def __init__(self, number):
        self.number = number

    def __str__(self):
        if self.number > 100 or self.number < 0:
            raise ValueError("Number outside the range of 0 - 100")
        coding = zip(
            [100, 90, 50, 40, 10, 9, 5, 4, 1],
            ["C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"]
        )
        result = []
        for d, r in coding:
            while self.number >= d:
                result.append(r)
                self.number -= d
        return ''.join(result)


class ToArabian:
    def __init__(self, number):
        self.number = number

    def __str__(self):
        help_list = ["None", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]
        return str(help_list.index(self.number))


print(ToRoman(80))
print(ToArabian("IX"))
