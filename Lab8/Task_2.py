import random


class Coin:
    __sideup = ""

    def __init__(self):
        self.__sideup = ["head", "tails"][random.randint(0, 1)]

    def __str__(self):
        return f"{self.__sideup}"

    def toss(self, __sideup=""):
        self.__sideup = ["head", "tails"][random.randint(0, 1)]
        return f"{self.__sideup}"


coin = Coin()
print(f"Значення coin при створенні {coin}")
for i in range(0, int(input("Скільки разів підкидаємо монету?\n"))):
    print(coin.toss())
