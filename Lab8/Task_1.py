class Bank:
    __balance = 1000

    def __str__(self):
        return f"Баланс: {self.__balance}"

    def add_money(self, __money):
        self.__balance += __money
        print(f"Операція успішна\nБаланс: {self.__balance}")

    def take_money(self, __money):
        if self.__balance > __money:
            self.__balance -= __money
            print("Операція успішна")
            return
        print("Недостатньо коштів!!!")


bank = Bank()
print(bank)
while True:
    menu = int(input("Що саме бажаєте зробити?\n1.Переглянути рахунок\n2.Покласти на рахунок\n3.Зняти "
                     "кошти\n4.Вихід\n"))
    if menu == 4:
        break
    if menu == 1:
        print(bank)
    elif menu == 2:
        bank.add_money(int(input("Введіть сумму:")))
    elif menu == 3:
        bank.take_money(int(input("Введіть сумму:")))
