class Dog:
    pets_class = ""
    nature = ""
    breed = ""

    def __init__(self, __age, __name):
        self.__age = __age
        self.__name = __name

    def get_info(self):
        print(f"{str(self.__name)} - {str(self.__age)} років")

    def get_voice(self):
        print(f"{self.__name} Гавкає")


class German_Shepherd(Dog):
    pets_class = "mammal"
    nature = "Доброзичливий, агресивний при небезпеці"
    breed = "German Shepherd"

    def __init__(self, __age, __name):
        Dog.__init__(self, __age, __name)
        self.__age = __age
        self.__name = __name

    def get_circle(self):
        print(f"{self.__name} Тримає кільце в щелепі")


class Bulldog(Dog):
    pets_class = "mammal"
    nature = "Агресивний при небезпеці"
    breed = "Bulldog"

    def __init__(self, __age, __name):
        Dog.__init__(self, __age, __name)
        self.__age = __age
        self.__name = __name

    def get_bed(self):
        print(f"{self.__name} лежить на спині")


class Pets(Bulldog, German_Shepherd):
    list = []
    Dog_1 = German_Shepherd(4, "Dog_1")
    list.append(Dog_1)
    Dog_2 = Bulldog(8, "Dog_2")
    list.append(Dog_2)


Pets.list[0].get_info()
Pets.list[0].get_circle()
Pets.list[0].get_voice()
print("\n")
Pets.list[1].get_info()
Pets.list[1].get_bed()
Pets.list[1].get_voice()
