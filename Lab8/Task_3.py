class Car:
    def __init__(self, __brand, __model, __year, __speed=0):
        self.__brand = __brand
        self.__model = __model
        self.__year = __year
        self.__speed = 0

    def accelerate(self):
        self.__speed += 5

    def brake(self):
        self.__speed -= 5

    def get_speed(self):
        return f"{str(self.__speed)}"

    def get_car_info(self):
        return f"{str(self.__brand)} {str(self.__model)} {str(self.__year)}"


car = Car("Volkswagen", "Golf", 2015)
print(car.get_car_info())

for i in range(0, 5):
    car.accelerate()
    print(f"speed {car.get_speed()}")

for i in range(0, 5):
    car.brake()
    print(f"speed {car.get_speed()}")
