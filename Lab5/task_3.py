def is_point_in_circle(x_0, y_0, r, x, y): return (x - x_0) ** 2 + (y - y_0) ** 2 <= r ** 2


print(f"Введіть координати центру кола та радіус")
x0 = int(input("X0 - "))
y0 = int(input("Y0 - "))
r = int(input("R - "))

for i in range(0, 3):
    print(f"Координати точки {i + 1}")
    pX = int(input("X - "))
    pY = int(input("Y - "))
    print("Точка належить колу" if is_point_in_circle(x0, y0, r, pX, pY) else "Точка не належить колу")
