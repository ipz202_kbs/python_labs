import random
import time


def check_type_input():
    while True:
        a = input("->")
        if a.isdigit():
            return int(a)
        else:
            print("Invalid number!")


def wrapper(list_in, bottom_in, upper_in):
    start_time = time.time()
    print(cut_list(list_in, bottom_in, upper_in))
    return time.time() - start_time


def cut_list(list_in, bottom_in, upper_in):
    final_list = []
    if max(list_in)-upper_in < min(list_in) + bottom_in or min(list_in)+bottom_in > max(list_in) - upper_in:
        print("Вихід за межі мін. так макс. значення!")
        return 0
    for i in range(len(list_in)):
        if list_in[i] >= min(list_in) + bottom_in and list_in[i] <= max(list_in) - upper_in:
            final_list.append(list_in[i])
    return final_list


print("Введіть розмір списку")
n = check_type_input()

lst = []
for i in range(0, n):
    lst.append(random.randint(-50, 51))
print(lst)

print("Ведіть нижню межу")
bottom = check_type_input()

print("Введіть верхню межу")
upper = check_type_input()

print(f"Час виконання програми - {wrapper(lst, bottom, upper)}")


