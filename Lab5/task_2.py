import math


def hypotenuse_length(a, b): return math.sqrt(a ** 2 + b ** 2)


lst = list()
for i in range(0, 3):
    print(f"Трикутник {i + 1}")
    leg_a = int(input("Введіть катет a - "))
    leg_b = int(input("Введіть катет b - "))
    lst.append(hypotenuse_length(leg_a, leg_b))
    print(f"Довжина гіпотенузи {i + 1} - {lst[i]}")
print(f"Найдовша гіпотенуза {lst.index(max(lst)) + 1} - {max(lst)}")
print(f"Найкоротша гіпотенуза {lst.index(min(lst)) + 1} - {min(lst)}")