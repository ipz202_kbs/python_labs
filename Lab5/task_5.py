def true_numbers(num_n, nums_in):
    final_nums = []
    for i in range(0, num_n + 1):
        flag = True
        for j in range(0, len(nums_in)):
            if i % nums_in[j] != 0:
                flag = False
                break
        if flag:
            final_nums.append(i)
    return final_nums


def check_type_input():
    while True:
        a = input("->")
        if a.isdigit():
            return int(a)
        else:
            print("Invalid number!")


print("Введіть n: ")

n = check_type_input()
nums = []
print("Оберіть кількість чисел: ")  # запитуємо значення
nums_count = check_type_input()
for i in range(0, nums_count):
    nums.append(check_type_input())

print("Nums which complete all conditions: " + str(true_numbers(n, nums)))  # результат
