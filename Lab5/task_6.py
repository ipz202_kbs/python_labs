import time

def check_type_input():
    while True:
        a = input("->")
        if a.isdigit():
            return int(a)
        else:
            print("Invalid number!")


def wrapper(m_in, n_in):
    start_time = time.time()
    print(max_divisors(m_in, n_in))
    return time.time() - start_time


def max_divisors(m_in, n_in):
    divisors_count_list = []
    for i in range(m_in, n_in + 1):
        divisors_count = 0
        for j in range(1, i + 1):
            if i % j != 0:
                continue
            divisors_count += 1
        divisors_count_list.append([i, divisors_count])
    return list(filter(lambda x: x[1] == max(divisors_count_list, key=lambda elem: elem[1])[1], divisors_count_list))


print("Введіть інтервал від M до N")
print("M - ")
m = check_type_input()
print("N - ")
n = check_type_input()
if m > n:
    buf = m
    m = n
    n = buf
start_time = time.time()
print(f"Час виконання програми - {wrapper(m, n)}")






