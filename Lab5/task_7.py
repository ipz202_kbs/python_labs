import time


def check_type_input():
    while True:
        a = input("->")
        if a.isdigit():
            if int(a) > 0:
                return int(a)
            else:
                print("Invalid number!")
                return
        else:
            print("Invalid number!")


def wrapper(n_in, print_option):
    start_time = time.time()
    prime_numbers(n_in, print_option)
    return time.time() - start_time


def prime_numbers(n_in, print_option):
    prime_list = []
    for i in range(0, n_in + 1):
        divisors_count = 0
        for j in range(1, i + 1):
            if i % j != 0:
                continue
            divisors_count += 1
        if divisors_count == 2:
            prime_list.append(i)
    if print_option == 1:
        print(prime_list)
    elif print_option == 2:
        for item in prime_list:
            print(str(item))
    elif print_option == 3:
        print(len(prime_list))
    else:
        print("Invalid option!!")


print("N")
n = check_type_input()
print("Як вивести результат?\n1)Списком\n2)Рядками в стовпчик\n3)Вивести кількість простих чисел")
option = check_type_input()
print(f"Час виконання програми - {wrapper(n,option)}")


