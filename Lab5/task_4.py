import math


def quadrangle_area(a_in, b_in, c_in, d_in):
    p = (a_in + b_in + c_in + d_in) / 2
    return math.sqrt((p-a_in) * (p-b_in) * (p-c_in) * (p-d_in))


print(f"Введіть сторони чотирикутника")
a = int(input("A - "))
b = int(input("B - "))
c = int(input("C - "))
d = int(input("D - "))
print(f"Площа чотирикутника - {round(quadrangle_area(a, b, c, d), 2)}")
