class Alphabet:
    lang = "ua"
    letters = ['а', 'б', 'в', 'г', 'ґ', 'д', 'е', 'є', 'ж', 'з', 'и', 'і', 'ї', 'й',
               'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч',
               'ш', 'щ', 'ь', 'ю', 'я']

    def __init__(self, lang, letters):
        self.__lang = lang
        self.__letters = letters

    def print_alphabet(self):
        print(self.__letters)

    def letters_num(self):
        return len(self.__letters)

    def is_ua_lang(self, some_text):
        for i in some_text.lower():
            if self.__letters.count(i) == 0:
                return "Ні"
        return "Так"


class EngAlphabet(Alphabet):
    __lang = "en"
    __letters = []

    def __init__(self, lang, letters):
        super().__init__(lang, letters)
        self.__lang = lang
        self.__letters = letters
        self.__en_letters_num = len(letters)

    def is_en_lang(self, some_text):
        for i in some_text.lower():
            if self.__letters.count(i) == 0:
                return "Ні"
        return "Так"

    def letters_num(self):
        return self.__en_letters_num

    @staticmethod
    def example():
        return 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean \n' \
               'massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec \n' \
               'quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec \n' \
               'pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, \n' \
               'venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.\n'


engAlphabet = EngAlphabet("en", ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'])
engAlphabet.print_alphabet()
ukrAlphabet = Alphabet(Alphabet.lang, Alphabet.letters)

print(f"Літер в англійському алфавіті: {engAlphabet.letters_num()}")
print(f"Дітер в українському алфавіті: {ukrAlphabet.letters_num()}")

print(f"Чи належить літера J до англійського алфавіту: {engAlphabet.is_en_lang('J')}")
print(f"Чи належить літера Щ до англійського алфавіту: {ukrAlphabet.is_ua_lang('Щ')}")

print(f"Текст англійською мовою:\n{EngAlphabet.example()}")
