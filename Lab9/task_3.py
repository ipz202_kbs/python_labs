class Apple:
    __states = ["Відсутнє", "Цвітіння", "Зелене", "Червоне"]

    __next_index = 0
    __instance_list = []

    def __init__(self):
        self._index = Apple.__next_index
        Apple.__next_index += 1
        self._state = Apple.__states[0]
        Apple.__instance_list.append(self)

    def grow(self):
        self._state = Apple.__states[(Apple.__states.index(self._state) + 1) % len(Apple.__states)]

    def is_ripe(self):
        return self._state == Apple.__states[-1]

    @staticmethod
    def apple_base():
        print(f"Кількість яблук: {len(Apple.__instance_list)}")
        for apple in Apple.__instance_list:
            if apple.is_ripe():
                print(f"Яблуко №{apple.__dict__['_index']} стигле")
            else:
                print(f"Яблуко №{apple.__dict__['_index']} не стигле")


class AppleTree:
    def __init__(self, number):
        self.apples = []
        for i in range(number):
            self.apples.append(Apple())

    def grow_all(self):
        for apple in self.apples:
            apple.grow()

    def are_all_ripe(self):
        if len(self.apples) == 0:
            return False

        for apple in self.apples:
            if not apple.is_ripe():
                return False
        return True

    def give_away_all(self):
        self.apples.clear()


class Gardener:
    def __init__(self, name, tree: AppleTree):
        self.name = name
        self._tree = tree

    def work(self):
        self._tree.grow_all()

    def harvest(self):
        if not self._tree.are_all_ripe():
            return print("Не можна зібрати урожай, не всі яблука стиглі")
        print(f"Садівник {self.name} зібрав {len(self._tree.apples)} яблук")
        self._tree.give_away_all()


apple1 = Apple()
apple2 = Apple()
Apple.apple_base()

tree1 = AppleTree(10)
gardener1 = Gardener('Petrov Petro', tree1)

gardener1.work()
gardener1.harvest()
gardener1.work()
gardener1.work()
gardener1.harvest()
