class Human:
    default_name = 'Bogdan'
    default_age = 0

    def __init__(self, name, age, money, house=None):
        if isinstance(name, str):
            self.name = name
        else:
            self.name = self.default_name

        if isinstance(age, int):
            self.age = age
        else:
            self.age = self.default_age

        self.__money = money
        self.__house = house

    def info(self):
        print(f"Ім'я: {self.name}\n"
              f"Вік: {self.age}\n"
              f"Гроші: {self.__money}$\n"
              f"Будинок: {self.__house}")

    @staticmethod
    def default_info():
        print(f"{Human.default_name}, {Human.default_age}")

    def __make_deal(self, price, house):
        self.__money -= price
        self.__house = house
        return print(f"Будинок куплено!\nУ вас залишилось {self.__money}$")

    def earn_money(self, money):
        self.__money += money
        print(f"На ваш рахунок додано {money}$. Баланс - {self.__money}$")

    def buy_house(self, house, discount=10):
        price = house._price - house._price * (discount / 100)
        if self.__money <= price:
            return print(f"Недостатньо коштів!")
        self.__make_deal(price, house)


class House:
    def __init__(self, area, price):
        self._area = area
        self._price = price

    def final_price(self, discount):
        discount = discount.replace('%', '')
        discount_total = 100 - int(discount.strip())
        print(self._price * discount_total / 100)


class SmallHouse(House):
    def __init__(self, price, area=40):
        super().__init__(area, price)
        self.price = price


Human.default_info()

human = Human("Ivan Ivanov", 20, 10000)
human.info()
smallHouse = SmallHouse(price=200000)

human.buy_house(house=smallHouse,  discount=15)
human.earn_money(300000)

human.buy_house(house=smallHouse,  discount=15)
human.info()
