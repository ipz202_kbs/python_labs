import User as user


class Admin(user.User):
    def __init__(self, first_name="Unknown", last_name="Admin",
                 nickname="admin", age=12, privileges=None):

        super().__init__(first_name, last_name, nickname, age)


class Privileges:
    def __init__(self, privileges=None):
        if privileges is None:
            privileges = ["Allowed to add message", "Allowed to delete users", "Allowed to ban users"]

        if not isinstance(privileges, list):
            raise TypeError

        if not isinstance(all(privileges), str):
            raise ValueError

        self.privileges = privileges

    def show_privileges(self):
        for item in self.privileges:
            print(item)