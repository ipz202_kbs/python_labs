from User import User
from privadmin import Admin, Privileges
from unittest import TestCase as UnitTest


class WebSiteTest(UnitTest):
    def testWrongTypes(self):
        with self.assertRaises(TypeError):
            User(1, 2, 3, 4)

    def testWrongValues(self):
        with self.assertRaises(ValueError):
            User(nickname='')

    def testWrongValues2(self):
        with self.assertRaises(ValueError):
            User(age=-10)

    def testPrivilegesWrongValue(self):
        with self.assertRaises(ValueError):
            Privileges(['priv1', 'priv2', 3])

    def testPrivilegesWrongType(self):
        with self.assertRaises(TypeError):
            Privileges(1)

    def testPrivilegesWrongType2(self):
        with self.assertRaises(TypeError):
            Admin(priv=1)
