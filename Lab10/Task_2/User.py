class User:
    def __init__(self, first_name="Unknown", last_name="User",
                 nickname="player", age=12):
        if not isinstance(first_name, str) or \
                not isinstance(last_name, str) or \
                not isinstance(nickname, str) or \
                not isinstance(age, int):
            raise TypeError

        if first_name != '' and last_name != '' \
                and nickname != '' and age > 0:
            self.first_name = first_name
            self.last_name = last_name
            self.nickname = nickname
            self.age = age
            self.login_attempts = 0
        else:
            raise ValueError

    def describe_user(self):
        print(self.first_name + " " + self.last_name)

    def greeting_user(self):
        print("Hello " + self.nickname)

    def increment_login_attempts(self):
        self.login_attempts += 1

    def reset_login_attempts(self):
        self.login_attempts = 0

    def get_login_attempts(self):
        return f'{self.login_attempts}'
