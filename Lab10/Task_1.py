from unittest import TestCase as UnitTest


class Shop:
    def __init__(self, shopName="noname", shopProducts="default", numberOfUnits=0):
        if isinstance(shopName, str):
            if shopName != '':
                self.shop_name = shopName
            else:
                raise ValueError
        else:
            raise TypeError

        if isinstance(shopProducts, str):
            if shopProducts != '':
                self.shop_products = shopProducts
            else:
                raise ValueError
        else:
            raise TypeError

        if isinstance(numberOfUnits, int):
            if numberOfUnits >= 0:
                self.number_of_units = numberOfUnits
            else:
                raise ValueError
        else:
            raise TypeError

    def describe_shop(self):
        print(self.shop_name)
        print(self.store_type)

    def open_shop(self):
        print("open")

    def set_number_of_units(self, number):
        if isinstance(number, int):
            if number >= 0:
                self.number_of_units = number
            else:
                raise ValueError
        else:
            raise TypeError

    def increment_number_of_units(self, increment_units):
        if isinstance(increment_units, int):
            if increment_units >= 0:
                self.number_of_units += increment_units
            else:
                raise ValueError
        else:
            raise TypeError


class Discount(Shop):
    def __init__(self, shopName = "noname", shopProducts = "default", numberOfUnits = 0, discountProducts = None):
        if discountProducts is None:
            discountProducts = ['chicken', 'beef', 'pork']
        else:
            if not isinstance(discountProducts, list):
                raise TypeError
            elif not isinstance(all(discountProducts), str):
                raise ValueError

        super().__init__(shopName, shopProducts, numberOfUnits)
        self.discountProducts = discountProducts

    def get_discount_products(self):
        print("\nПродукти зі знижкою: ")
        for i in self.discountProducts:
            print(f"{i}")

class ShopTester(UnitTest):
    def testWrongNameValue(self):
        with self.assertRaises(ValueError):
            d = Discount("")

    def testWrongNameType(self):
        with self.assertRaises(TypeError):
            d = Discount(shopName=123)

    def testWrongProductValue(self):
        with self.assertRaises(ValueError):
            d = Discount(shopProducts='')

    def testWrongProductType(self):
        with self.assertRaises(TypeError):
            d = Discount(shopProducts=123)

    # Тестування класу Shop
    def testWrongNumberOfUnitsValue(self):
        with self.assertRaises(ValueError):
            s = Shop(numberOfUnits=-3)

    def testWrongNumOfUnitsType(self):
        with self.assertRaises(TypeError):
            s = Shop(numberOfUnits='цацфа')

    def testIncrementWrongValue(self):
        s = Shop()
        with self.assertRaises(ValueError):
            s.increment_number_of_units(-100)

    def testIncrementWrongType(self):
        s = Shop()
        with self.assertRaises(TypeError):
            s.increment_number_of_units('5')

    def testSetWrongValue(self):
        s = Shop()
        with self.assertRaises(ValueError):
            s.set_number_of_units(-3)

    def testSetWrongType(self):
        s = Shop()
        with self.assertRaises(TypeError):
            s.set_number_of_units('25')
