"""question_list.py"""

import datetime
import random

TOTAL_QUESTIONS_LIST = []
AllQuestionsList = []
GlobalScoreCounter = 0
CurrentQuestion = None
StartDate = datetime.datetime.now()


class Question:
    is_quiz_started = True
    AnswerCounter = 0

    def __init__(self, qText, answerArr, trueAnswerNum):
        self.qText = qText
        self.answerArr = answerArr
        self.trueAnswerNum = trueAnswerNum
        self.AnswerCounter = self.AnswerCounter

    def ShowAnswerVariants(self):
        text = ""
        for i in range(0, len(self.answerArr)):
            text += f"{i + 1}) {self.answerArr[i]}.\n"
        return text

    def ShowTrueAnswer(self, userAnswer):
        if int(userAnswer) == int(self.trueAnswerNum + 1):
            Question.AnswerCounter += 1
            return "Правильно!"
        return f"Неправильно, правильна відповідь: \n{self.answerArr[self.trueAnswerNum]}"

    def EnrollAnswer(self, userAnswer):
        return self.ShowTrueAnswer(userAnswer)


TOTAL_QUESTIONS_LIST.extend([
    Question("Що таке Python?",
             ["Не знаю.", "Вид змій", "Мова програмування низького рівня", "Мова програмування високого рівня"],
             3),
    Question("Що виведе даний код?\n printf('Hello World!')",
             ["Виникне помилка", "Виведе 'Hello World!'", "Виведе число 12", "Виведе 'Hello'"],
             0),
    Question("Python найшвидша мова програмування?",
             ["Так", "Ні", "Можливо", "Не знаю"],
             1),
    Question("Що виведе даний код?\n print(2+4)",
             ["2 4", "24", "6", "0"],
             2),
    Question("Чи потрібно в кінці рядка коду ставити ';'?",
             ["Обов'язково", "Бажано", "Непотрібно", "Заборонено"],
             2),
    Question("Яка відмінність між кортежем та списком?",
             ["Кортежі займають більше місця",
              "Список захищений від змін",
              "Кортеж захищений від змін",
              "Кортежі більше не підтримуються"],
             2),
    Question("Що є правдою про Python?",
             ["Python найшвидша мова програмування",
              "Для виконання коду на Python використовується інтерпретатор C",
              "Після пожного рядка треба писати крапку з комою ",
              "Більшість вірусів написано на Python"],
             1),
    Question("Чи підтримує python_3.9 switch case конструкцію?",
             ["Так", "Не знаю", "Можливо", "Ні"],
             3),
    Question("Що швидше Python чи JavaScript?",
             ["Python", "Швидкість однакова", "JavaScript", "Швидкіть не можливо виміряти"],
             2),
    Question("Що виведе даний код?\n print(x==1)",
             ["false", "true", "Помилку", "True"],
             2)
])


def IsAnswer(str):
    return str == "1" or str == "2" or str == "3" or str == "4"


def SelectRandomQuestion():
    if len(AllQuestionsList) > 0:
        i = random.randrange(0, len(AllQuestionsList))
        current_question = AllQuestionsList.pop(i)
        return current_question
    return None


def ShowTotalResults():
    return f"Результат: {Question.AnswerCounter} із {len(TOTAL_QUESTIONS_LIST)}\n" \
           f"Дата проходження тесту: {datetime.datetime.now()}\n" \
           f"Тривалість проходження тесту: {datetime.datetime.now() - StartDate}\n" \
           f"Для проходження заново натисніть /restart"


def Restart():
    Question.AnswerCounter = 0
    Question.is_quiz_started = True
    AllQuestionsList.clear()
    for item in TOTAL_QUESTIONS_LIST:
        AllQuestionsList.append(item)

