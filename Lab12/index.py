"""index.py"""

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor

import keyboard
import question_list

bot = Bot(token="5084463120:AAHAYOGhsmEtX-9tikJ1bbVNhaHYaWhHKdI")
dp = Dispatcher(bot)

question_list.Restart()
question_list.CurrentQuestion = question_list.SelectRandomQuestion()  # поточне запитання


@dp.message_handler(commands=['c2'])
async def process_c2_command(message: types.Message):
    await message.reply("Привет!", reply_markup=keyboard.KbQuizRow)


async def show_question(message: types.Message):
    if question_list.CurrentQuestion is not None:
        await message.reply(f"{question_list.CurrentQuestion.qText}\n\n{question_list.CurrentQuestion.ShowAnswerVariants()}", reply_markup=keyboard.KbQuizRow)
        return
    await end_quiz(message)


async def generate_new_question(message: types.Message):
    question_list.CurrentQuestion = question_list.SelectRandomQuestion()
    print(f"({message.from_user.username}) Правильних відповідей - {question_list.Question.AnswerCounter}")
    if question_list.CurrentQuestion is not None:
        await message.reply(
            f"{question_list.CurrentQuestion.qText}\n\n{question_list.CurrentQuestion.ShowAnswerVariants()}",
            reply_markup=keyboard.KbQuizRow)
    else:
        await end_quiz(message)


@dp.message_handler(commands=['start', 'restart'])
async def restart(message: types.Message):
    print(f"Користувач {message.from_user.username} почав тест")
    question_list.Restart()
    await generate_new_question(message)


@dp.message_handler()
async def get_answer(message: types.Message):
    if question_list.Question.is_quiz_started:
        if question_list.IsAnswer(message.text):
            num = int(message.text)
            answer = question_list.CurrentQuestion.EnrollAnswer(num)
            await message.answer(answer)
            await generate_new_question(message)
        else:
            await message.answer("Введіть правильний варіант!")


async def end_quiz(message: types.Message):
    question_list.Question.is_quiz_started = False
    print(f"{message.from_user.username} закінчив тест\n")
    await message.answer(f"Кінець!\n\n{question_list.ShowTotalResults()}", reply_markup=keyboard.KbNone)


if __name__ == '__main__':
    executor.start_polling(dp)
