"""keyboard.py"""

from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

testKey = ReplyKeyboardMarkup(resize_keyboard=True)
testKey.add(KeyboardButton('Test'))

KbQuizRow = ReplyKeyboardMarkup(resize_keyboard=True)\
    .row(KeyboardButton('1'), KeyboardButton('2'), KeyboardButton('3'), KeyboardButton('4'))


KbNone = ReplyKeyboardMarkup(resize_keyboard=True).row(KeyboardButton('/restart'))