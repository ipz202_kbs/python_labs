import random
lst = list()
arr_len = 20
for i in range(0, arr_len):
    lst.append(random.randint(-50, 50))

odd_elements = 0
i = 1
while i < arr_len:
    odd_elements += lst[i]
    i += 2
print(f"List - {lst}")
print(f"Odd elements sum - {odd_elements}")
