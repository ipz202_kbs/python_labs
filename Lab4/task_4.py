import random
lst = list()
odd_lst = list()
arr_len = 30
for i in range(0, arr_len):
    lst.append(random.randint(-100, 100))
    if lst[i] % 2 != 0:
        odd_lst.append(lst[i])
print(lst)
print(f"Max element - {max(lst)}[{lst.index(max(lst))}]")
if len(odd_lst) == 0:
    print("No odd elements!")
else:
    odd_lst.sort(reverse=True)
    print(f"Odd list - {odd_lst}")
