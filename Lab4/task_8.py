import random
import math


def split_list(input_list, wanted_parts=1):
    length = len(input_list)
    return [input_list[j * length // wanted_parts: (j + 1) * length // wanted_parts] for j in range(wanted_parts)]


def abs_sum(a):
    sum_abs = 0
    for j in range(0, len(a)):
        sum_abs += math.fabs(a[j])
    return sum_abs


lst = list()
arr_len = 30
for i in range(0, arr_len):
    lst.append(round(random.uniform(-100, 100), 2))
print("Початковий список\n", lst)

list_of_lists = split_list(lst, 10)

print("Список списків\n", list_of_lists)

list_of_lists.sort(key=abs_sum)

print("Відсортований список списків\n", list_of_lists)

for i in range(0, len(list_of_lists)):
    sum_of_item = 0
    for item in list_of_lists[i]:
        sum_of_item += math.fabs(item)
    print(f"Сумма абсолютних значень {i} списку - {round(sum_of_item, 2)}")
