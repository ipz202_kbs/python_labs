import datetime
import os.path

if not os.path.isfile('guest_book.txt'):
    with open("guest_book.txt", "wt", encoding="utf-8") as guest_book:
        time = datetime.datetime.today().strftime("%Y-%m-%d-%H.%M.%S")
        guest_book.write(f"Дата та час створення файлу - {time}.\n\n\n")

with open("guest_book.txt", "rt", encoding="utf-8") as guest_book:
    lines = guest_book.readlines()
    if len(lines) == 2:
        lines = lines[:-1]
    else:
        lines = lines[:-2]

while True:
    print("1 - Додати користувача. 0 - вихід.")
    option = int(input("->"))
    if option == 0:
        break
    name = str(input())
    print(f"Вітаємо {name}!")
    time = datetime.datetime.today().strftime("%Y-%m-%d-%H.%M.%S")
    lines.append(f"Вітаємо {name}! - {time}.\n")

time = datetime.datetime.today().strftime("%Y-%m-%d-%H.%M.%S")
lines.append(f"\nnЧас останньої зміни - {time}.")

with open("guest_book.txt", "wt", encoding="utf-8") as guest_book:
    guest_book.writelines(lines)


