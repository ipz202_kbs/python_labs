import datetime
import os.path
import time

with open("article.txt", "rt", encoding="utf-8") as article:
    text = article.read().lower()

find = str(input("Що шукаємо?\n->")).lower()
print(f"{find} зустрічається {text.count(find)} разів.")

if not os.path.isfile('result.txt'):
    with open("result.txt", "wt", encoding="utf-8") as result:
        create_time = datetime.datetime.today().strftime("%Y-%m-%d-%H.%M.%S")
        result.write(f"Дата та час створення файлу - {create_time}.\n\n\n")

with open("result.txt", "rt", encoding="utf-8") as result:
    lines = result.readlines()
    if len(lines) == 2:
        lines = lines[:-1]
    else:
        lines = lines[:-2]

start_time = time.time()
count = text.count(find)
work_time = time.time() - start_time
lines.append(f"{find} зустрічається {count} разів.\nЧас пошуку - {work_time}\n")

change_time = datetime.datetime.today().strftime("%Y-%m-%d-%H.%M.%S")
lines.append(f"\nЧас останньої зміни - {change_time}.")

with open("result.txt", "wt", encoding="utf-8") as result:
    result.writelines(lines)


