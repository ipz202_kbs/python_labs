import csv
import collections

with open("marks.lab6.csv", "r", encoding="utf-8") as marks:
    oll_data = csv.reader(marks)
    rows_file = [line for line in oll_data]

print('Всього студентів - ', len(rows_file))

points = [line[4].replace(',', '.') for line in rows_file]
for value in collections.Counter(points).items():
    print(f"Оцінка {value[0]} - {value[1]} разів")

time = []
for line in (line[3].split(' ') for line in rows_file):
    if len(line) == 4:
        time.append(int(round((int(line[0])*60 + int(line[2])) / 60)))
        continue
    time.append(int(round((int(line[0])*60) / 60)))

for time_test in range(1, max(time) + 1):
    sum = 0
    all = 0
    for i in range(len(time)):
        if time[i] == time_test:
            sum += float(points[i])
            all += 1
    if sum > 0:
        print(f"За {time_test}хв набрали {round(sum / all, 2)} балів.")

right_ans = []
for i in range(5, 25):
    sum = 0
    for j in range(len(points)):
        if rows_file[j][i] == '0,50':
            sum += 1
            continue
    right_ans.append(f"Питання {i+1} - {round(sum / 170, 2) * 100}% правильно, {100 - round(sum / 170, 2) * 100}% неправильно.\n")

with open("stats.txt", "wt", encoding="utf-8") as stats:
    stats.writelines(right_ans)
    stats.write("\n")

    points_to_time = []
    for i in range(len(points)):
        points_to_time.append([i, round(float(points[i]) / float(time[i]), 2), points[i]])
    points_to_time.sort(key=lambda x: x[1])
    for i in range(0, 5):
        stats.write(f'Студент №{points_to_time[i][0]}: оцінка - {points_to_time[i][2]}, співвідношення "оцінка/час" - {points_to_time[i][1]}\n')

