data_list = list()
with open("learning_python.txt", "rt", encoding="utf-8") as file:
    for line in file:
        print(line[:len(line) - 1])
        data_list.append(line)

data_list.sort(key=lambda x: len(x))

print("\nSorted list\n")
for line in data_list:
    print(line[:len(line) - 1])

with open("learning_python_sorted.txt", "wt", encoding="utf-8") as file:
    file.writelines(data_list)
