import requests
from bs4 import BeautifulSoup
"""Task_1 """
URL = 'https://rozklad.ztu.edu.ua/'
response = requests.get(URL, headers={'Accept':
                                          'text/html,application/xhtml+xml,application/xml;q=0.9,'
                                          'image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'})
soup = BeautifulSoup(response.text, 'html.parser')
fikt = soup.find_all('div', class_="auto-clear")[1]
fikt_groups = fikt.find_all('a', class_="collection-item")
all_rooms = {}

for i in range(len(fikt_groups)):
    response = requests.get(URL + fikt_groups[i]['href'])
    soup = BeautifulSoup(response.text, 'html.parser')
    rooms = soup.find_all('span', class_="room")
    for room in rooms:
        if room:
            current_room = room.text.strip()
            if current_room in all_rooms:
                all_rooms[current_room] = all_rooms[current_room] + 1
            else:
                all_rooms[current_room] = 1

all_rooms = dict(sorted(all_rooms.items(), key=lambda item: item[1], reverse=True))
print('5 найчастіше використовуваних аудиторій на ФІКТі серед всіх груп та курсів:')
i = 0
for key, value in all_rooms.items():
    print(f'\t Аудиторія "{key}" використовується {value} р.')
    i += 1
    if i == 4:
        break

"""Task_2"""
URL_ROOMS = 'https://rozklad.ztu.edu.ua/schedule/room'
response = requests.get(URL_ROOMS)
soup = BeautifulSoup(response.text, 'html.parser')
rooms = soup.find_all('a')
rooms = [i for i in rooms if i.text.startswith("ОЦ")]
rooms_dict = {}
for i in range(len(rooms)):
    response = requests.get(URL + rooms[i]['href'])
    soup = BeautifulSoup(response.text, 'html.parser')
    count_lessons = len(soup.find_all('td', class_="content"))
    rooms_dict[rooms[i].text] = count_lessons
rooms_dict = dict(sorted(rooms_dict.items(), key=lambda item: item[1]))

print('\n3 аудиторії обчислювального центру, які частіше всього залишаються не задіяними:')
i = 0
for key, value in rooms_dict.items():
    print(f'\t Аудиторія "{key}" використовується {value} р.')
    i += 1
    if i == 3:
        break

"""Task_3"""

URL_GROUP = 'https://rozklad.ztu.edu.ua/schedule/group/%D0%86%D0%9F%D0%97-20-2'
response = requests.get(URL_GROUP)
soup = BeautifulSoup(response.text, 'html.parser')
rooms = soup.find_all('span', class_="room")
my_group_rooms = {}
for room in rooms:
    if room:
        current_room = room.text.strip()
        if current_room in my_group_rooms:
            my_group_rooms[current_room] = my_group_rooms[current_room] + 1
        else:
            my_group_rooms[current_room] = 1

my_group_rooms = dict(sorted(my_group_rooms.items(), key=lambda item: item[1], reverse=True))
print('\n5 аудиторій, у яких частіше всього проходять заняття у групи ІПЗ-20-2:')
i = 0
for key, value in my_group_rooms.items():
    print(f'\t Аудиторія "{key}" використовується {value} р.')
    i += 1
    if i == 4:
        break
