a = int(input("Enter a:"))
b = int(input("Enter b:"))
c = float(input("Enter c:"))
d = float(input("Enter d:"))

lst = list()

lst.append(a + b)
lst.append(a - b)
lst.append(a * c)
lst.append(d / c)
lst.append(a ** c)
lst.append(a // c)
lst.append(d % a)

print(lst)

print("Number of list items: ", len(lst))

for item in lst:
    if (item % 2) == 0:
        print(item)

tmp = lst[1]
lst[1] = lst[4]
lst[4] = tmp

print(lst)

name = str(input("Enter name:"))

print("Лабораторну роботу виконав: ", name,
      ".\n В ході виконання лабораторної роботи я ойомитися алгоритмами послідовної (лінійної)структури, \n з процедурами запуску програм, які реалізують ці алгоритмина мові Python; \nзнайомство з інтегрованим середовищем розробки –integrated development environment (IDLE)")