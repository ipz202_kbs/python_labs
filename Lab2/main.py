import math

lst = list()
"""
# Task_1
lst.append(int(input("Enter a:")))
lst.append(int(input("Enter b:")))
lst.append(int(input("Enter c:")))
for item in lst:
    if 1 <= item <= 3:
        print(item)
lst.clear()

# Task_2
year = int(input("Enter year:"))
if year % 2 == 0 and year % 400 == 0:
    print("366 days in this year")
elif not year % 100 != 0:
    print("365 days in this year")

# Task_3
price = int(input("Enter price:"))
if 500 <= price <= 1000:
    print(price*0.97)
elif price > 1000:
    print(price*0.95)

# Task_4
lst.append(int(input("Enter a:")))
lst.append(int(input("Enter b:")))
lst.append(int(input("Enter c:")))
lst.append(int(input("Enter d:")))
print("Min number -", min(lst), "Cos -", math.cos(min(lst)))
lst.clear()

# Task_5
lst.append(int(input("Enter a:")))
lst.append(int(input("Enter b:")))
lst.append(int(input("Enter c:")))
print("Max number -", max(lst), "Sin -", math.sin(max(lst)))
lst.clear()

# Task_6
print("Remember, A and B sides must be equal!!! If it not programm will not work ")
a = int(input("Enter side a: "))
b = int(input("Enter side b: "))
c = int(input("Enter side c: "))
if a == b:
    p = (a + b + c) / 2
    s = math.sqrt(p * (p - a) * (p - b) * (p - c))
    print(f"S = {s} m2")
    if s % 2 == 0:
        print(f"S/2 = {s / 2}")
    else:
        print("Не можу ділити на 2!")
else:
    print("You enter incorrect values!!!")

# Task_7
Month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
month = int(input("Введіть номер місяця: "))
if 0 < month < 13:
    print("Назва місяцю англійською", Month[month-1])
else:
    print("Номер місяця не вірний")

# Task_8
positive_count = 0
a = int(input("Enter a:"))
if a > 0:
    positive_count += 1
b = int(input("Enter b:"))
if b > 0:
    positive_count += 1
c = int(input("Enter c:"))
if c > 0:
    positive_count += 1
print("Кількість позитивних чисел -", positive_count)

# Task_9
a = int(input("Enter a, a < b!"))
b = int(input("Enter b:"))
if a < b:
    sum = 0
    for i in range(a, b + 1):
        sum += i
    print(f"Сума від {a} до {b} = {sum}")
else:
    print("You enter incorrect values!!!")

# Task_10
a = int(input("Enter a, a < b!"))
b = int(input("Enter b:"))
if a < b:
    sum = 0
    for i in range(a, b + 1):
        sum += math.pow(i, 2)
    print(f"Сума квадратів від {a} до {b} = {sum}")
else:
    print("You enter incorrect values!!!")

# Task_11

# Task_12
a = int(input("Enter a, a < b!"))
b = int(input("Enter b:"))
if a < b:
    sum = 0
    i = a
    while i <= b:
        sum += i
        i += 1
    print(f"Сума від {a} до {b} = {sum}")
else:
    print("You enter incorrect values!!!")

# Task_13
a = int(input("Enter a, 0 <= a <= 50!"))
if 0 <= a <= 50:
    sum = 0
    for i in range(a, 51):
        sum += math.pow(i, 2)
    print(f"Сума квадратів від {a} до 50 = {sum}")
else:
    print("You enter incorrect values!!!")   

# Task_14
n = int(input("Enter N, N > 1!"))
if n > 1:
    k = -1
    while 5 ** k <= n:
        k += 1
    print(f"Найменше К - {k}")
else:
    print("You enter incorrect values!!!")

# Task_15
n = int(input("Enter N:"))
for i in range(n):
    if i**2 > n:
        print(f"Перше число, більше {n} - {i ** 2}")
        break
"""
# Task_16
