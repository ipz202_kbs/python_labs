input_string = input("Введіть текст: ")
number_of_change = 0
final_string = ""
for letter in input_string:
    if letter == "а":
        number_of_change += 1
        letter = "о"

    final_string += letter

print("Новий текст: ", final_string)
print("Кількість замін: ", number_of_change)
print("Кількість символів в рядку: ", len(input_string))
