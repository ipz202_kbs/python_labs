input_string = str(input("Enter string:")).lower()
number_of_change = 0

final_string = ""
for word in input_string:
    if word == ':':
        number_of_change += 1
        word = '%'

    final_string += word

print(final_string)
print("Number of replacements: ", number_of_change)
