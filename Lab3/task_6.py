input_string = input("Введіть текст: ")
number_of_change = 0
final_string = ""
for letter in input_string:
    if letter == "о":
        number_of_change += 1
        letter = ""

    final_string += letter

print("Новий текст: ", final_string)
print(f"Видалено {number_of_change} символів")
