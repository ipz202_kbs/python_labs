input_string = str(input("Enter text: "))
all_words = input_string.split()

final_string = ""
for word in all_words:
    word = word[0].upper() + word[1:]
    final_string += word + " "

print(f"Result: {final_string}")
