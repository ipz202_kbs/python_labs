input_string = input("Введіть текст: ")
chars = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z']
k = 0
for letter in input_string:
    for char in chars:
        if letter.isalpha() and letter.lower() == char:
            k += 1
print("Кількість приголосних: ", k)
