input_string = str(input("Enter text: "))
vowels = ["a", "o", "e", "i", "u", "y"]
vowel_count = 0
for letter in input_string:
    for vowel in vowels:
        if letter.lower() == vowel:
            vowel_count += 1
print(f"Count of vowels: {vowel_count}")
